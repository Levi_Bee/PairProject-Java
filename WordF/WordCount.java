package WordF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class WordCount {
	private int CharSum =0; // 字符个数
	private int WordsSum =0; // 单词总数
	private int LineSum =0; // 有效行数
	private Map<String,Integer> WordFre; //单词词频

	String text;
	public WordCount(String text) {
		this.text = text;
	}
	public int countWords() {
		String str=text;
		String[] words=str.split("\\s*[^0-9a-zA-Z]+");//调用正则表达式中spilt()方法来切分字段，将字符串中的单词提取出来
		for(String s:words) {
			if(s.matches("[a-zA-Z]{4,}[a-zA-Z0-9]*")) { //调用matchs方法来判断这个字符串是否在给定的正则表达式匹配
				WordsSum++;
			}
		}
		return WordsSum;
	}
	public int countChar() {//统计字符数量
		for(int i=0;i<text.length();i++) {
			char c=text.charAt(i);
			if(c >= 32 && c <= 127 ||c == 10) {
				CharSum++;
			}
		}
		return CharSum;
	}
	public int countLine() {//统计行数
		String[] lines=text.split("\r\n");
		for(int i=0;i<lines.length;i++) {
			if(lines[i].trim().length()!=0) { //调用trim（）方法将头尾空格去掉，来判断行数
			    LineSum++;
			}
		}
		return LineSum;
	}
	public  boolean isWord(String s)//判断是否是单词
	{
		if(s.matches("[a-zA-Z]{4}[a-zA-Z0-9]*"))//利用正则表达式判断是否是单词
			return true;
		else
			return false;
	}

	public List<Entry<String, Integer>> WordFre()
	{
		WordFre= new HashMap<String, Integer>();
		String t = text;
	
		String[] words = t.split("\\s"); // 将字符串进行分割
		
		for (int i = 0; i < words.length; i++) {
				if (isWord(words[i])) { // 判断是否为单词，调用isWord函数
					
					words[i] = words[i].trim().toLowerCase();//将大写字母变成小写字母
					
					
					if (WordFre.get(words[i]) == null) { // 判断之前Map中是否出现过该字符串
						
						
						WordFre.put(words[i], 1);// 如果为新单词，放入map中作为key值，value设为1
						
					} else
						
						WordFre.put(words[i], WordFre.get(words[i]) + 1);//如果出现过的单词则将value值＋1
				}
		}
		
		List<Map.Entry<String, Integer>> list = new ArrayList<Map.Entry<String, Integer>>(WordFre.entrySet());//用list列表储存键值
		
		list.sort(new Comparator<Map.Entry<String, Integer>>() {//对list排序
			
			@Override
			
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {	
				
				if (o1.getValue() == o2.getValue()) {//判断词频是否相等
					
					return o1.getKey().compareTo(o2.getKey());//字典序排列
				}
				return o2.getValue() - o1.getValue();//降序排列
			}

		});
		
		return list;
	}
	}
	
	
	
	   

	




