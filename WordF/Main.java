package WordF;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Main {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub
		FileRead fi=new FileRead();
		Scanner result = new Scanner(System.in);
		  System.out.println("请输入你要WordFrequent的路径:");
		  String path = result.next();
		  result.close();
		  String text =fi.FileInput(path);
		  WordCount wc=new WordCount(text);
		  List<Map.Entry<String, Integer>> WordFre=wc.WordFre();
		  int WordSum= WordFre.size();
		  int CharSum = wc.countChar(); // 获取字符数
		  int WordsSum = wc.countWords();//获取单词数
		  int LineSum = wc.countLine();//获取行数

		  System.out.println("characters:"+CharSum+"\n");
		  System.out.println("words:"+WordsSum+"\n");
		  System.out.println("lines:"+LineSum+"\n");
		  for (int i = 0; i < WordSum; i++) {
			System.out.println("["+WordFre.get(i).getKey()+"] :"+WordFre.get(i).getValue()+"\n");
		}
		  
}
}
